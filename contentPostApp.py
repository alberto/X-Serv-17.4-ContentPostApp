import webapp


class contentPostApp(webapp.webApp):
    content = {'/': 'Root page', '/page': 'A page'}

    def parse(self, request):
        metodo = request.split(' ')[0][0:]
        petition = request.split(' ')[1]
        if metodo == "POST":
            body = request.split('recurso=')[1]
        else:
            body = None
        return metodo,petition,body

    def process(self, resourceName):
        metodo,petition,body=resourceName
        if metodo=='GET':
            if petition in self.content.keys():
                httpCode = "200 OK"
                htmlBody="<html><body><h1>El contentido de la pagina es "+self.content[petition] \
                +"<form method='POST'><input type='text' placeholder='Escriba el Post' name='recurso'</body></html>"
            else:
                httpCode = "200 OK"
                htmlBody = "<html><body><h1>No hay informacion de este recurso actualmente"\
                           "<form method='POST'><input type='text' placeholder='Escriba el Post' name='recurso'</body></html>"
        elif metodo=="POST":
            self.content[petition]=body
            print("la petition es:"+str(body))
            httpCode = "200 OK"
            htmlBody = "<html><body><h1>El contentido de la pagina es "+self.content[petition] \
                +"<form method='POST'><input type='text' placeholder='Escriba el Post' name='recurso'</body></html>"
        else:
            httpCode = "404 ERROR"
            htmlBody = "<html><body>Error found</body></html>"
        return (httpCode, htmlBody)

if __name__ == "__main__":

    testWebApp = contentPostApp("localhost", 1234)